# Pistes de travail UX & UI - PROCESS DE CONTACT-ELU.   

## LISTE DES ACTIONS MANDATORY À DEVELOPPER 

**1. Pop up ou modal de rédaction du message.**
   1. Choix du _theme_ de contact.
   2. Choix de la _typologie_ de contact.
   3. GO.   

**2. Pop up ou modal de remerciements.**

## ELEMENTS DE REFLEXION : DETAIL ETAPES DE TRAVAIL.
### Départ du processus depuis la "blue card" la page [informer.html](http://www.polipart.fr/informer.html).   

**ELEMENT 1**   
Pour contacter un élu, on clique un portrait ou une icône ou les deux ?   

**ELEMENT 2 ?**   
Si on clique un portrait, présente t'on une [fiche d'identité](https://sketch.cloud/s/4mOmz/GmmOgpO) ou peut on directement rédiger [un message](https://sketch.cloud/s/4mOmz/OmmbMPm) ?   

**ELEMENT 3 Sélection thématique**   
Déterminer le nombre de mots à afficher et fonction de quoi [15 à 25 cf liste](https://drive.google.com/open?id=1w3MW8YqLLcd4lSjTDO9k7zgEd9-ifmvU).   
Nuage de mots disponible en design.   
Possibilité d'ajout d'une thématique.   
Choix de la présence d'une Iconographie ou pas.   
Choix de l'affichage de famille de couleur ou pas.

**ELEMENT 4 Sélection typologique**   
Choisir et valider les [dénominations et icônes](https://sketch.cloud/s/4mOmz/DPrpqJW).   

**ELEMENT 5 Consulter les questions**   
Voir si un module existe dans la liste de ceux qu'utilise [Decidim](https://github.com/decidim/decidim).   

**ELEMENT 6 Identification pour validation**   
Open discussion experts - Tenant et aboutissant technique ?   
Facebook/Twitter login, Github/Gitlab login ?   
Login par email? si création compte - Info: Nom. prénom. adresse. email.

**ELEMENT 7 Message public/privé**   
Open discussion experts - Tenant et aboutissant technique ?   

**ELEMENT 8 Remerciements**   
Remerciements au nom de Polipart.   
Remerciements au nom de l'élu si ...
