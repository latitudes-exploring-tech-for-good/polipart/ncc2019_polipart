# Polipart

+ *Thème :* accès à l'information.
+ *Phase d'avancement actuelle :* conception.
+ *Compétences associés :* developpement web, design, vision produit.

##### #0 | Résultats obtenus lors de la Nuit du Code Citoyen
Formulaire de ""contact-elu"" disponible pour chaque institution. Maintenant pour 99,6% de la population il est plus facile de s'adresser à ses élus.

##### #1 | Présentation de Polipart
Polipart est une association 1901 qui a vocation à faciliter la consultation d'informations publiques et à simplifier l'implication auprès des élus et des institutions.

En savoir plus : [polipart.fr](http://polipart.fr/) et [document "ressources"](./ressources/)

##### #2 | Problématique
La démocratie représentative repose aussi sur le lien existant entre la population et ses élus. Il est important que le lien ne soit pas trop ténu sinon le sentiment de représentativité n'est plus aussi fort que l'exige notre modèle démocratique. 
Comment renforcer la relation existante entre la population et ses représentants ? 

##### #3 | Le défi proposé
Réaliser une interface en ligne qui permette de contacter simplement son/ses représentants élus. L'outil doit simplifier le processus de traitement des sollicitations et faire gagner du temps aux élus. 

##### #4 | Livrables
Une fonctionnalité de contact intégrée à la page de visualisation des élus de l'application Polipart.   

1/ Développement web :
+ Système de messagerie pour l'envoi.
+ Systeme d'identification (login/password ou "X-connect").
+ Bonus: Encryption (cacher/chiffrer l'email de l'émétteur pour qu'il ne soit pas visible de l'élu).

2/ Design :
+ Parcours utilisateur (chemin de fer).
+ Design system des éléments.

##### #5 | Ressources à disposition pour résoudre le défi
+ Une [présentation de Polipart](./ressources/presentation_polipart.md).
+ Une [présentation de Contact élu](./ressources/presentation_contact-elu.md).

+ Le code source de l'app via un repo gitlab (stack= RubyOnRails, Postgresql, Postgis, AlgoliaPace).
+ Un schema sur le process de fonctionnement de l'app.
+ Les [sketchs du projet](https://sketch.cloud/s/Q0QqG).
+ Une [proposition de réalisation](https://drive.google.com/open?id=15pT7C56ezGxCu28YGtWF_lTyrSKL3y42).

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateur.trice.s, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Brice : fondateur de Polipart et porteur de projet.
+ Yannick : co-fondateur de Latitudes, et en charge de la préparation du défi Polipart.