### PROJET : 
Aider les élus à traiter les sollicitations qu'ils reçoivent de leurs administrés (jusqu'à 400 emails par JOUR).

### PROBLÈME : 
> Les élus sont sollicités par divers canaux (emails, facebook, twitter…).   
Les élus sont sollicités sur des thématiques qui ne les concernent pas.   
Le traitement des sollicitations prend du temps aux élus et à leurs équipes.

### SOLUTION :
Un formulaire de contact qui filtre et trie selon des thématique et la nature de la sollicitation afin de gagner du temps dans le traitement. 

---

### PRODUIT :
**Elu :** Un email dans lequel apparait la thématique et le type de sollicitation. La redirection et la réattribution des emails en fonction des critères nommés. Un feedback statistique et analytique des sollicitations.   
**Population :** Une interface en ligne avec laquelle on s’adresse à son élu. On mentionne une thématique (transports, impôts, scolarité…), le type de sollicitation (grief, question, proposition…).

### BUT :
Avoir un formulaire unique et uniformisé de prise de contact.   
Rediriger les sollicitations vers les personnes pertinentes (commission, adjoint, secrétaire…).   
Décharger les collaborateurs du travail de tri des sollicitations. 

### BÉNÉFICES :
"Gagner du temps dans le traitement des sollicitations".   
"Répondre plus rapidement".   
"Satisfaire ses administrés".

### CIBLES :
Cible 1 (étape 1) : Les députés (577).   
Cible 2 (étape 2) : Maire villes > 10 000 hab. (1 000).   
Cible 3 (étape 3) : Responsables de région et département (120).