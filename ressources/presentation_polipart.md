# [POLIPART - L'outil de politique participative.](https://www.polipart.fr)
(version v-vraiment beta :stuck_out_tongue_winking_eye:)

---
### CONCEPT
**Why :**   
Faire vivre notre démocratie au 3ème millénaire pour l’améliorer.   
Réconcilier politique et population pour construire notre avenir.   
**How :**   
En améliorant la transparence de l’action publique on pourra rétablir la crédibilité politique, et peut être la confiance.   
**What :**  
Une plateforme en ligne pour faire circuler l’information et nous créer des moyens d’implication.

---
### PROJET   
**1). Un outil pour simplifier l’accès à l’information publique.**    
1. [x] Qui sont mes représentants (élus/institutions) ? :tada:    
2. [ ] Que font mes élus (décisions / délibérations) ?     
3. [ ] Que font mes institutions (actualité) ?    
**S’informer / Identifier / Comprendre.**   

**2). Un outil pour faciliter l’implication auprès des élus et des institutions.**   
1. [ ] Contacter (institution/ élus/ conseillers).
2. [ ] Proposer (réflexion/ réunion / délibération).
3. [ ] Accéder aux projets de délibérations et se prononcer.   
**S’impliquer / Participer / Collaborer / Contribuer.**   

---
### ETAT DE L’ART
A date, le site n'a que la fonctionnalité du point 1.1 : identifier ses représentants.   
- 99,6% de la pop peut identifier ses élus.   
- 100% de la pop métropolitaine peut identifier son maire, pdt de département, pdt de région, et député.   
- ¼ de la pop peut identifier **tous** ses représentants avec portraits et logo. 

A ce stade - le site a vocation à susciter de l’implication pour du financement et de la data (portrait/logo et délibérations).
